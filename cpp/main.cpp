#include <fstream>
#include <string>
#include <iostream>

#include <CommonCrypto/CommonDigest.h>

#include "md5.h"

int main()
{
    std::ifstream inputFile("passwords.txt");
    std::string password;
    unsigned char digest[16];

    if(inputFile.is_open()) {
        while(inputFile >> password)
            std::cout << password << " " << md5(password) << std::endl;
    }
    inputFile.close();

    return 0;
}