C++:
real	0m6.901s
user	0m5.144s
sys	0m0.856s

BASH:
154976: 770c48c219e7db4123d1d698e3788d96
^C

real	8m8.694s
user	3m5.793s
sys	5m14.992s


Python:
real	0m7.180s
user	0m5.429s
sys	0m0.961s

django:
In [1]: from django.contrib.auth.hashers import make_password

In [2]: make_password("123456")
Out[2]: 'pbkdf2_sha256$36000$wzi7098ecXQO$5StEa0bsyZUgX+g8B8yN05KdQkH26D6xykUST9Nm4iM='


```
time hashcat -D 1 -m 0 -a 0 md5_passwords.txt passwords.txt -O
hashcat (v4.2.1) starting...

OpenCL Platform #1: Apple
=========================
* Device #1: Intel(R) Core(TM) i5-7360U CPU @ 2.30GHz, 4096/16384 MB allocatable, 4MCU
* Device #2: Intel(R) Iris(TM) Plus Graphics 640, skipped.

Hashes: 3 digests; 3 unique digests, 1 unique salts
Bitmaps: 16 bits, 65536 entries, 0x0000ffff mask, 262144 bytes, 5/13 rotates
Rules: 1

Applicable optimizers:
* Optimized-Kernel
* Zero-Byte
* Precompute-Init
* Precompute-Merkle-Demgard
* Meet-In-The-Middle
* Early-Skip
* Not-Salted
* Not-Iterated
* Single-Salt
* Raw-Hash

Minimum password length supported by kernel: 0
Maximum password length supported by kernel: 31

Watchdog: Temperature abort trigger disabled.

Dictionary cache built:
* Filename..: passwords.txt
* Passwords.: 999999
* Bytes.....: 8529110
* Keyspace..: 999999
* Runtime...: 0 secs

e10adc3949ba59abbe56e057f20f883e:123456
610ef919a73003ec8b73e434fb3116d4:lp0520
Approaching final keyspace - workload adjusted.

3a3a5c3f10e14cc9d5e92127a0ee0880:vjht008

Session..........: hashcat
Status...........: Cracked
Hash.Type........: MD5
Hash.Target......: md5_passwords.txt
Time.Started.....: Sat Aug  3 20:59:07 2019 (0 secs)
Time.Estimated...: Sat Aug  3 20:59:07 2019 (0 secs)
Guess.Base.......: File (passwords.txt)
Guess.Queue......: 1/1 (100.00%)
Speed.Dev.#1.....:  5723.4 kH/s (0.33ms) @ Accel:1024 Loops:1 Thr:1 Vec:4
Recovered........: 3/3 (100.00%) Digests, 1/1 (100.00%) Salts
Progress.........: 999999/999999 (100.00%)
Rejected.........: 9/999999 (0.00%)
Restore.Point....: 999433/999999 (99.94%)
Candidates.#1....: vjq445 -> vjht008

Started: Sat Aug  3 20:59:06 2019
Stopped: Sat Aug  3 20:59:08 2019

real	0m2.277s
user	0m0.526s
sys	0m0.104s
```

# hashcat dictionary attack on md5

```
time hashcat -D 2 -m 0 -a 0 md5_passwords.txt passwords.txt -O
hashcat (v4.2.1) starting...

OpenCL Platform #1: Apple
=========================
* Device #1: Intel(R) Core(TM) i5-7360U CPU @ 2.30GHz, skipped.
* Device #2: Intel(R) Iris(TM) Plus Graphics 640, 384/1536 MB allocatable, 48MCU

Hashes: 3 digests; 3 unique digests, 1 unique salts
Bitmaps: 16 bits, 65536 entries, 0x0000ffff mask, 262144 bytes, 5/13 rotates
Rules: 1

Applicable optimizers:
* Optimized-Kernel
* Zero-Byte
* Precompute-Init
* Precompute-Merkle-Demgard
* Meet-In-The-Middle
* Early-Skip
* Not-Salted
* Not-Iterated
* Single-Salt
* Raw-Hash

Minimum password length supported by kernel: 0
Maximum password length supported by kernel: 31

Watchdog: Temperature abort trigger disabled.

Dictionary cache built:
* Filename..: passwords.txt
* Passwords.: 999999
* Bytes.....: 8529110
* Keyspace..: 999999
* Runtime...: 0 secs

Approaching final keyspace - workload adjusted.

e10adc3949ba59abbe56e057f20f883e:123456
610ef919a73003ec8b73e434fb3116d4:lp0520
3a3a5c3f10e14cc9d5e92127a0ee0880:vjht008

Session..........: hashcat
Status...........: Cracked
Hash.Type........: MD5
Hash.Target......: md5_passwords.txt
Time.Started.....: Sat Aug  3 21:04:35 2019 (0 secs)
Time.Estimated...: Sat Aug  3 21:04:35 2019 (0 secs)
Guess.Base.......: File (passwords.txt)
Guess.Queue......: 1/1 (100.00%)
Speed.Dev.#2.....:  8168.5 kH/s (8.40ms) @ Accel:64 Loops:1 Thr:256 Vec:1
Recovered........: 3/3 (100.00%) Digests, 1/1 (100.00%) Salts
Progress.........: 999999/999999 (100.00%)
Rejected.........: 9/999999 (0.00%)
Restore.Point....: 786433/999999 (78.64%)
Candidates.#2....: zhansuluzhanel -> vjht008

Started: Sat Aug  3 21:04:25 2019
Stopped: Sat Aug  3 21:04:35 2019

real	0m10.280s
user	0m0.307s
sys	0m0.291s
```

# hashcat bruteforce attack on md5

time hashcat -D 2 -m 0 -a 3 md5_passwords.txt
hashcat (v4.2.1) starting...

OpenCL Platform #1: Apple
=========================
* Device #1: Intel(R) Core(TM) i5-7360U CPU @ 2.30GHz, skipped.
* Device #2: Intel(R) Iris(TM) Plus Graphics 640, 384/1536 MB allocatable, 48MCU

Hashes: 3 digests; 3 unique digests, 1 unique salts
Bitmaps: 16 bits, 65536 entries, 0x0000ffff mask, 262144 bytes, 5/13 rotates

Applicable optimizers:
* Zero-Byte
* Early-Skip
* Not-Salted
* Not-Iterated
* Single-Salt
* Brute-Force
* Raw-Hash

Minimum password length supported by kernel: 0
Maximum password length supported by kernel: 256

ATTENTION! Pure (unoptimized) OpenCL kernels selected.
This enables cracking passwords and salts > length 32 but for the price of drastically reduced performance.
If you want to switch to optimized OpenCL kernels, append -O to your commandline.

Watchdog: Temperature abort trigger disabled.

The wordlist or mask that you are using is too small.
This means that hashcat cannot use the full parallel power of your device(s).
Unless you supply more work, your cracking speed will drop.
For tips on supplying more work, see: https://hashcat.net/faq/morework

Approaching final keyspace - workload adjusted.

Session..........: hashcat
Status...........: Exhausted
Hash.Type........: MD5
Hash.Target......: md5_passwords.txt
Time.Started.....: Sun Aug  4 08:44:03 2019 (0 secs)
Time.Estimated...: Sun Aug  4 08:44:03 2019 (0 secs)
Guess.Mask.......: ?1 [1]
Guess.Charset....: -1 ?l?d?u, -2 ?l?d, -3 ?l?d*!$@_, -4 Undefined
Guess.Queue......: 1/15 (6.67%)
Speed.Dev.#2.....:     3649 H/s (0.10ms) @ Accel:16 Loops:7 Thr:256 Vec:1
Recovered........: 0/3 (0.00%) Digests, 0/1 (0.00%) Salts
Progress.........: 62/62 (100.00%)
Rejected.........: 0/62 (0.00%)
Restore.Point....: 1/1 (100.00%)
Candidates.#2....: O -> X

The wordlist or mask that you are using is too small.
This means that hashcat cannot use the full parallel power of your device(s).
Unless you supply more work, your cracking speed will drop.
For tips on supplying more work, see: https://hashcat.net/faq/morework

Approaching final keyspace - workload adjusted.

Session..........: hashcat
Status...........: Exhausted
Hash.Type........: MD5
Hash.Target......: md5_passwords.txt
Time.Started.....: Sun Aug  4 08:44:03 2019 (0 secs)
Time.Estimated...: Sun Aug  4 08:44:03 2019 (0 secs)
Guess.Mask.......: ?1?2 [2]
Guess.Charset....: -1 ?l?d?u, -2 ?l?d, -3 ?l?d*!$@_, -4 Undefined
Guess.Queue......: 2/15 (13.33%)
Speed.Dev.#2.....:   175.2 kH/s (0.09ms) @ Accel:16 Loops:7 Thr:256 Vec:1
Recovered........: 0/3 (0.00%) Digests, 0/1 (0.00%) Salts
Progress.........: 2232/2232 (100.00%)
Rejected.........: 0/2232 (0.00%)
Restore.Point....: 36/36 (100.00%)
Candidates.#2....: Oa -> Xq

The wordlist or mask that you are using is too small.
This means that hashcat cannot use the full parallel power of your device(s).
Unless you supply more work, your cracking speed will drop.
For tips on supplying more work, see: https://hashcat.net/faq/morework

Approaching final keyspace - workload adjusted.

Session..........: hashcat
Status...........: Exhausted
Hash.Type........: MD5
Hash.Target......: md5_passwords.txt
Time.Started.....: Sun Aug  4 08:44:04 2019 (0 secs)
Time.Estimated...: Sun Aug  4 08:44:04 2019 (0 secs)
Guess.Mask.......: ?1?2?2 [3]
Guess.Charset....: -1 ?l?d?u, -2 ?l?d, -3 ?l?d*!$@_, -4 Undefined
Guess.Queue......: 3/15 (20.00%)
Speed.Dev.#2.....:  6378.7 kH/s (0.12ms) @ Accel:16 Loops:7 Thr:256 Vec:1
Recovered........: 0/3 (0.00%) Digests, 0/1 (0.00%) Salts
Progress.........: 80352/80352 (100.00%)
Rejected.........: 0/80352 (0.00%)
Restore.Point....: 1296/1296 (100.00%)
Candidates.#2....: Oar -> Xqx

The wordlist or mask that you are using is too small.
This means that hashcat cannot use the full parallel power of your device(s).
Unless you supply more work, your cracking speed will drop.
For tips on supplying more work, see: https://hashcat.net/faq/morework

Approaching final keyspace - workload adjusted.

Session..........: hashcat
Status...........: Exhausted
Hash.Type........: MD5
Hash.Target......: md5_passwords.txt
Time.Started.....: Sun Aug  4 08:44:04 2019 (0 secs)
Time.Estimated...: Sun Aug  4 08:44:04 2019 (0 secs)
Guess.Mask.......: ?1?2?2?2 [4]
Guess.Charset....: -1 ?l?d?u, -2 ?l?d, -3 ?l?d*!$@_, -4 Undefined
Guess.Queue......: 4/15 (26.67%)
Speed.Dev.#2.....:   130.1 MH/s (2.15ms) @ Accel:16 Loops:15 Thr:256 Vec:1
Recovered........: 0/3 (0.00%) Digests, 0/1 (0.00%) Salts
Progress.........: 2892672/2892672 (100.00%)
Rejected.........: 0/2892672 (0.00%)
Restore.Point....: 46656/46656 (100.00%)
Candidates.#2....: Uari -> Xqxv

Approaching final keyspace - workload adjusted.

Session..........: hashcat
Status...........: Exhausted
Hash.Type........: MD5
Hash.Target......: md5_passwords.txt
Time.Started.....: Sun Aug  4 08:44:04 2019 (1 sec)
Time.Estimated...: Sun Aug  4 08:44:05 2019 (0 secs)
Guess.Mask.......: ?1?2?2?2?2 [5]
Guess.Charset....: -1 ?l?d?u, -2 ?l?d, -3 ?l?d*!$@_, -4 Undefined
Guess.Queue......: 5/15 (33.33%)
Speed.Dev.#2.....:   197.6 MH/s (7.22ms) @ Accel:16 Loops:15 Thr:256 Vec:1
Recovered........: 0/3 (0.00%) Digests, 0/1 (0.00%) Salts
Progress.........: 104136192/104136192 (100.00%)
Rejected.........: 0/104136192 (0.00%)
Restore.Point....: 1679616/1679616 (100.00%)
Candidates.#2....: Upf5u -> Xqxvq

e10adc3949ba59abbe56e057f20f883e:123456
610ef919a73003ec8b73e434fb3116d4:lp0520
Approaching final keyspace - workload adjusted.


Session..........: hashcat
Status...........: Exhausted
Hash.Type........: MD5
Hash.Target......: md5_passwords.txt
Time.Started.....: Sun Aug  4 08:44:05 2019 (14 secs)
Time.Estimated...: Sun Aug  4 08:44:19 2019 (0 secs)
Guess.Mask.......: ?1?2?2?2?2?2 [6]
Guess.Charset....: -1 ?l?d?u, -2 ?l?d, -3 ?l?d*!$@_, -4 Undefined
Guess.Queue......: 6/15 (40.00%)
Speed.Dev.#2.....:   267.3 MH/s (2.96ms) @ Accel:32 Loops:8 Thr:256 Vec:1
Recovered........: 2/3 (66.67%) Digests, 0/1 (0.00%) Salts
Progress.........: 3748902912/3748902912 (100.00%)
Rejected.........: 0/3748902912 (0.00%)
Restore.Point....: 1679616/1679616 (100.00%)
Candidates.#2....: Wq4tpv -> Xqqfqx

3a3a5c3f10e14cc9d5e92127a0ee0880:vjht008

Session..........: hashcat
Status...........: Cracked
Hash.Type........: MD5
Hash.Target......: md5_passwords.txt
Time.Started.....: Sun Aug  4 08:44:19 2019 (53 secs)
Time.Estimated...: Sun Aug  4 08:45:12 2019 (0 secs)
Guess.Mask.......: ?1?2?2?2?2?2?2 [7]
Guess.Charset....: -1 ?l?d?u, -2 ?l?d, -3 ?l?d*!$@_, -4 Undefined
Guess.Queue......: 7/15 (46.67%)
Speed.Dev.#2.....:   245.0 MH/s (11.60ms) @ Accel:32 Loops:8 Thr:256 Vec:1
Recovered........: 3/3 (100.00%) Digests, 1/1 (100.00%) Salts
Progress.........: 13517193216/134960504832 (10.02%)
Rejected.........: 0/13517193216 (0.00%)
Restore.Point....: 0/1679616 (0.00%)
Candidates.#2....: ebmeran -> oowb3ye

Started: Sun Aug  4 08:44:00 2019
Stopped: Sun Aug  4 08:45:12 2019

real	1m12.052s
user	0m2.602s
sys	0m4.428s


# hashcat bruteforce attack on pbkdf2

$  time hashcat -D 2 -m 10000 -a 3 pbkdf2_passwords.txt
hashcat (v4.2.1) starting...

OpenCL Platform #1: Apple
=========================
* Device #1: Intel(R) Core(TM) i5-7360U CPU @ 2.30GHz, skipped.
* Device #2: Intel(R) Iris(TM) Plus Graphics 640, 384/1536 MB allocatable, 48MCU

Hashes: 3 digests; 3 unique digests, 3 unique salts
Bitmaps: 16 bits, 65536 entries, 0x0000ffff mask, 262144 bytes, 5/13 rotates

Applicable optimizers:
* Zero-Byte
* Brute-Force
* Slow-Hash-SIMD-LOOP

Minimum password length supported by kernel: 0
Maximum password length supported by kernel: 256

Watchdog: Temperature abort trigger disabled.

The wordlist or mask that you are using is too small.
This means that hashcat cannot use the full parallel power of your device(s).
Unless you supply more work, your cracking speed will drop.
For tips on supplying more work, see: https://hashcat.net/faq/morework

Approaching final keyspace - workload adjusted.

Session..........: hashcat
Status...........: Exhausted
Hash.Type........: Django (PBKDF2-SHA256)
Hash.Target......: pbkdf2_passwords.txt
Time.Started.....: Sun Aug  4 09:08:10 2019 (20 mins, 46 secs)
Time.Estimated...: Sun Aug  4 09:28:56 2019 (0 secs)
Guess.Mask.......: ?1 [1]
Guess.Charset....: -1 ?l?d?u, -2 ?l?d, -3 ?l?d*!$@_, -4 Undefined
Guess.Queue......: 1/15 (6.67%)
Speed.Dev.#2.....:        0 H/s (0.23ms) @ Accel:8 Loops:4 Thr:256 Vec:1
Recovered........: 0/3 (0.00%) Digests, 0/3 (0.00%) Salts
Progress.........: 186/186 (100.00%)
Rejected.........: 0/186 (0.00%)
Restore.Point....: 1/1 (100.00%)
Candidates.#2....: X -> X

The wordlist or mask that you are using is too small.
This means that hashcat cannot use the full parallel power of your device(s).
Unless you supply more work, your cracking speed will drop.
For tips on supplying more work, see: https://hashcat.net/faq/morework

Approaching final keyspace - workload adjusted.

Session..........: hashcat
Status...........: Exhausted
Hash.Type........: Django (PBKDF2-SHA256)
Hash.Target......: pbkdf2_passwords.txt
Time.Started.....: Sun Aug  4 09:28:56 2019 (26 mins, 10 secs)
Time.Estimated...: Sun Aug  4 09:55:06 2019 (0 secs)
Guess.Mask.......: ?1?2 [2]
Guess.Charset....: -1 ?l?d?u, -2 ?l?d, -3 ?l?d*!$@_, -4 Undefined
Guess.Queue......: 2/15 (13.33%)
Speed.Dev.#2.....:        4 H/s (0.11ms) @ Accel:8 Loops:2 Thr:256 Vec:1
Recovered........: 0/3 (0.00%) Digests, 0/3 (0.00%) Salts
Progress.........: 6696/6696 (100.00%)
Rejected.........: 0/6696 (0.00%)
Restore.Point....: 36/36 (100.00%)
Candidates.#2....: Xa -> Xq

The wordlist or mask that you are using is too small.
This means that hashcat cannot use the full parallel power of your device(s).
Unless you supply more work, your cracking speed will drop.
For tips on supplying more work, see: https://hashcat.net/faq/morework

Approaching final keyspace - workload adjusted.

Session..........: hashcat
Status...........: Exhausted
Hash.Type........: Django (PBKDF2-SHA256)
Hash.Target......: pbkdf2_passwords.txt
Time.Started.....: Sun Aug  4 09:55:06 2019 (44 mins, 51 secs)
Time.Estimated...: Sun Aug  4 10:39:57 2019 (0 secs)
Guess.Mask.......: ?1?2?2 [3]
Guess.Charset....: -1 ?l?d?u, -2 ?l?d, -3 ?l?d*!$@_, -4 Undefined
Guess.Queue......: 3/15 (20.00%)
Speed.Dev.#2.....:       90 H/s (0.34ms) @ Accel:8 Loops:2 Thr:256 Vec:1
Recovered........: 0/3 (0.00%) Digests, 0/3 (0.00%) Salts
Progress.........: 241056/241056 (100.00%)
Rejected.........: 0/241056 (0.00%)
Restore.Point....: 1296/1296 (100.00%)
Candidates.#2....: Xar -> Xqx

The wordlist or mask that you are using is too small.
This means that hashcat cannot use the full parallel power of your device(s).
Unless you supply more work, your cracking speed will drop.
For tips on supplying more work, see: https://hashcat.net/faq/morework

Approaching final keyspace - workload adjusted.

[s]tatus [p]ause [b]ypass [c]heckpoint [q]uit => s

Session..........: hashcat
Status...........: Running
Hash.Type........: Django (PBKDF2-SHA256)
Hash.Target......: pbkdf2_passwords.txt
Time.Started.....: Sun Aug  4 10:53:02 2019 (2 hours, 44 mins)
Time.Estimated...: Tue Mar 24 06:58:25 2020 (232 days, 18 hours)
Guess.Mask.......: ?1?2?2?2?2?2 [6]
Guess.Charset....: -1 ?l?d?u, -2 ?l?d, -3 ?l?d*!$@_, -4 Undefined
Guess.Queue......: 6/15 (40.00%)
Speed.Dev.#2.....:      559 H/s (10.06ms) @ Accel:8 Loops:2 Thr:256 Vec:1
Recovered........: 0/3 (0.00%) Digests, 0/3 (0.00%) Salts
Progress.........: 5505024/11246708736 (0.05%)
Rejected.........: 0/5505024 (0.00%)
Restore.Point....: 0/60466176 (0.00%)
Candidates.#2....: Oarier -> Oc7223

[s]tatus [p]ause [b]ypass [c]heckpoint [q]uit => ^C

real	270m51.501s
user	16m24.450s
sys	38m28.882s


# hashcat dictionary attack on pbkdf2

$ time hashcat -D 2 -m 10000 -a 0 pbkdf2_passwords.txt  passwords.txt
hashcat (v4.2.1) starting...

OpenCL Platform #1: Apple
=========================
* Device #1: Intel(R) Core(TM) i5-7360U CPU @ 2.30GHz, skipped.
* Device #2: Intel(R) Iris(TM) Plus Graphics 640, 384/1536 MB allocatable, 48MCU

Hashes: 3 digests; 3 unique digests, 3 unique salts
Bitmaps: 16 bits, 65536 entries, 0x0000ffff mask, 262144 bytes, 5/13 rotates
Rules: 1

Applicable optimizers:
* Zero-Byte
* Slow-Hash-SIMD-LOOP

Minimum password length supported by kernel: 0
Maximum password length supported by kernel: 256

Watchdog: Temperature abort trigger disabled.

Dictionary cache built:
* Filename..: passwords.txt
* Passwords.: 999999
* Bytes.....: 8529110
* Keyspace..: 999999
* Runtime...: 0 secs

[s]tatus [p]ause [b]ypass [c]heckpoint [q]uit => s

Session..........: hashcat
Status...........: Running
Hash.Type........: Django (PBKDF2-SHA256)
Hash.Target......: pbkdf2_passwords.txt
Time.Started.....: Sun Aug  4 13:39:41 2019 (21 secs)
Time.Estimated...: Sun Aug  4 15:07:41 2019 (1 hour, 27 mins)
Guess.Base.......: File (passwords.txt)
Guess.Queue......: 1/1 (100.00%)
Speed.Dev.#2.....:      570 H/s (8.80ms) @ Accel:8 Loops:2 Thr:256 Vec:1
Recovered........: 0/3 (0.00%) Digests, 0/3 (0.00%) Salts
Progress.........: 0/2999997 (0.00%)
Rejected.........: 0/0 (0.00%)
Restore.Point....: 0/999999 (0.00%)
Candidates.#2....: 123456 -> 231158

pbkdf2_sha256$36000$wzi7098ecXQO$5StEa0bsyZUgX+g8B8yN05KdQkH26D6xykUST9Nm4iM=:123456
pbkdf2_sha256$36000$5ZI2h8VP2VrZ$zhlEVEYdPthCpdlZPNIgoTbxqJTOFbiJMjCkHyEkDcA=:lp0520
Approaching final keyspace - workload adjusted.

pbkdf2_sha256$36000$Ax8dKgCTBmyo$IIj8oIM5HemI0qLwZq52FaYLzr4DmAH1LJwsLz4gplc=:vjht008

Session..........: hashcat
Status...........: Cracked
Hash.Type........: Django (PBKDF2-SHA256)
Hash.Target......: pbkdf2_passwords.txt
Time.Started.....: Sun Aug  4 13:39:41 2019 (49 mins, 22 secs)
Time.Estimated...: Sun Aug  4 14:29:03 2019 (0 secs)
Guess.Base.......: File (passwords.txt)
Guess.Queue......: 1/1 (100.00%)
Speed.Dev.#2.....:      570 H/s (1.64ms) @ Accel:8 Loops:2 Thr:256 Vec:1
Recovered........: 3/3 (100.00%) Digests, 3/3 (100.00%) Salts
Progress.........: 2999997/2999997 (100.00%)
Rejected.........: 0/2999997 (0.00%)
Restore.Point....: 983040/999999 (98.30%)
Candidates.#2....: vrs4983 -> vjht008

Started: Sun Aug  4 13:39:36 2019
Stopped: Sun Aug  4 14:29:04 2019

real	49m28.392s
user	1m0.759s
sys	1m53.776s