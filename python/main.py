#!/usr/bin/env python3

import hashlib

i=0
with open("passwords.txt") as passwords:
    for line in passwords:
        print(f"{i}: {hashlib.md5(line.encode('utf-8')).hexdigest()}")
        i += 1